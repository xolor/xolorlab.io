-function(){
  var $ = function(id){
    return {
      dom: document.querySelector(id),
      html: function(data){
        var me = this;
        if(typeof(data)=="string"){
          me.dom.innerHTML = data;
        }
        else {
          return me.dom.innerHTML;
        }
      },
      text: function(data){
        var me = this;
        if(typeof(data)=="string"){
          me.dom.innerText = data;
        }
        else {
          return me.dom.innerText;
        }
      },
      append: function(node){
        this.dom.appendChild(node);
      }
    }
  };
  var woo={};
  woo.throttle = function(action, delay){
    var me = this;
    var timer=null;
    return function(){
      var context=this, args=arguments;
      window.clearTimeout(timer);
      timer = window.setTimeout(function(){ action.apply(me, args); },delay);
    }
  };
  var vx = {};
  vx.rowid="{{o.rowid}}";
  vx.lazyload = {
    items: [],
    clear: function(){
      items = [];
    },
    run: function(){
      var me = this;
      var top = (document.documentElement.scrollTop || document.body.scrollTop);
      var line = top + document.documentElement.clientHeight;
      var rest = [];
      while(me.items.length>0){
        var img = me.items.pop();
        var t = img.offsetTop;
        if((t>=top) && (t<=line)) {
          img.onload = function(){
            var src = this.attributes["data-src"];
            if(this.src.indexOf(src.value)>-1){
              this.className = "o-normal";
              this.onclick = function(){ window.open(this.src) };
            }
            else {
              var g = this;
              this.style.cursor = "pointer";
              if(this.src.indexOf("../cdn/text.svg")>-1){
                this.className = "o-error";
              }
              this.onclick = function(){
                g.src = "../cdn/nil.svg";
                window.setTimeout(function() {
                  g.className = "o-load";
                }, 0);
                window.setTimeout(function() { 
                  g.className = "o-load";
                  g.src = src.value;
                }, 300);
              };
            }
          };
          img.onerror = function(){
            this.onclick = null;
            this.src="../cdn/text.svg";
          }
          img.className = "o-load";
          img.src = (img.attributes["data-src"]||{name:"",value:"../cdn/nil.svg"}).value;
        }
        else{
          rest.push(img);
        }
      }
      me.items = rest.reverse();
      if(me.items.length==0) {
        window.onscroll = null;
      }
    },
    load: function(id){
      var me = this;
      var nodes = $(id).dom.getElementsByTagName("img");
      var list = [];
      for(var i=0; i<nodes.length; i++){
        list.push(nodes[i]);
      }
      list.reverse();
      for(var i=0; i<list.length; i++){
        var img = list[i];
        img.className = "o-load";
        me.items.push(img);
      }
      window.onscroll = woo.throttle(function(){ me.run(); }, 500);
      this.run();
    }
  };
  vx.load = function(){
    var me = this;
    window.setTimeout(function(){ me.lazyload.load("#content"); }, 500);
  };
  vx.load();
}();